﻿using System;

namespace comp5002_10011581_assessment01
{
    class Program
    {
            static void Main(string[] args)
        {
            // Display Welcome Message " Welcome to Supermart "
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine("          Welcome to Supermart         ");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine("");

            // Declaration of all Variabes
            var name="";
            double GST = 0.15;
            double item1;
            string yesOrNo;
            double item2;
            double totalPrice = 0;
            double total = 0;

            // Ask User for write the Name
            Console.WriteLine("Write your Name here ");
            Console.WriteLine("");

            // Storing the User Name in a Variable
            name= Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine("Hi "+ name);
            Console.WriteLine("");

            // Input any two decimal number by user
            Console.WriteLine("Write any number with two decimal places");
            Console.WriteLine("");
            item1 = double.Parse(Console.ReadLine());

            // User decision for the decimal Number
            Console.WriteLine("Are you sure you want this number and add another number - Yes or No");
            Console.WriteLine("");
            yesOrNo = Console.ReadLine().ToLower();

            // Condition if user write 'yes'
            if (yesOrNo == "yes")
            {
                Console.WriteLine("Add Another Number");
                Console.WriteLine("");
                item2 = double.Parse(Console.ReadLine());
                totalPrice = item1 + item2; 
            }
            // Condition if User write 'No'
            else if (yesOrNo == "no")
            {
                totalPrice = item1;
                Console.WriteLine($"Your total number is {totalPrice}");
                Console.WriteLine("");
            }
            // Add GST to total number
            total = totalPrice + totalPrice*GST;
            Console.WriteLine("");
            Console.WriteLine($"Total of your itmes  = {totalPrice}");
            Console.WriteLine("");
            Console.WriteLine($"GST on your items    = {totalPrice*GST}");
            Console.WriteLine("");
            Console.WriteLine($"Grand Total with GST = {total}");
            Console.WriteLine("");
            
            // Display 'Thank You' message at end for User 
            Console.WriteLine("~~ Thank you for shopping with us, please come again ~~");   
        }
    }
}
